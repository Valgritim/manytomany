package com.manytomany.demo.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manytomany.demo.model.Reader;

@Repository
public interface ReaderRepository extends JpaRepository<Reader, Long>{
}
