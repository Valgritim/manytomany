package com.manytomany.demo.controller;

import java.util.Set;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manytomany.demo.exceptions.ResourceNotFoundException;
import com.manytomany.demo.model.Reader;
import com.manytomany.demo.model.Student;
import com.manytomany.demo.repository.ReaderRepository;
import com.manytomany.demo.repository.StudentRepository;

@RestController
@RequestMapping("api/readers") // lecturers endpoint
public class ReaderController {
	
	@Autowired
	private ReaderRepository readerRepository;
	@Autowired
	private StudentRepository studentRepository;
	
	@PostMapping()
	public Reader createrReader(@Valid @RequestBody Reader reader) {
		return this.readerRepository.save(reader);
	}
	
	@GetMapping()
	public Page<Reader> getReaders(Pageable pageable) {
		return this.readerRepository.findAll(pageable);
	}
	
	@GetMapping("/{id}")
	public Reader getReader(@PathVariable Long id) {
		return this.readerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Reader", id));
	}
	
	@PutMapping("/{id}")
	public Reader updateReader(@Valid @RequestBody Reader reader, @PathVariable Long id) {
		return this.readerRepository.findById(id).map((toUpdate) -> {
			toUpdate.setFirstName(reader.getFirstName());
			toUpdate.setLastName(reader.getLastName());
			toUpdate.setSalary(reader.getSalary());
			return this.readerRepository.save(toUpdate);
		}).orElseThrow(() -> new ResourceNotFoundException("Reader", reader.getId()));
	}
//	@PostMapping("/{id}/students")
//	public Reader createStudent(@RequestBody Reader reader, @PathVariable Long id) {
//		return this.readerRepository.findById(id).map((toAdd) ->{
//			toAdd.setStudents()
//		})
//	}
	
	@DeleteMapping("/{id}") 
	public ResponseEntity deleteReader(@PathVariable Long id) {
		return this.readerRepository.findById(id).map((toDelete) -> {
			this.readerRepository.delete(toDelete);
			return ResponseEntity.ok("Lecturer id " + id + " deleted");
		}).orElseThrow(() -> new ResourceNotFoundException("Reader", id));
	}
	
	@GetMapping("/{readerId}/students")
	public Set<Student> getStudents(@PathVariable Long readerId) {
		return this.readerRepository.findById(readerId).map((reader) -> {
			return reader.getStudents();
		}).orElseThrow(() -> new ResourceNotFoundException("Reader", readerId));
	}
}
